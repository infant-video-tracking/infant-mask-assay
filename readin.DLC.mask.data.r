##### Read in Knickmeyer mask data as processed by DeepLabCut
#frameTimes <- 1:dim(conf)[1]/fps #make for loop for this if needed
# 1:Nose; 2: Neck; 3:Chin; 4:Right shoulder; 5: Right Elbow; 6:Right Wrist; 7:Left Shoulder; 8:Left Elbow; 9:Left Wrist;  17:REye; 18:LEye; 19:REar; 20:LEar

DLC.headKeyPoints = c(1,17:20,3)   # same order as OP then 3 = Chin not in OP
DLC.upperBodyPoints = c(2,4:9) # same order as OP (incl. neck)
myKPs <- sort( c(DLC.headKeyPoints, DLC.upperBodyPoints))
# if (DLC_ONLY) {headKeyPoints <- DLC.headKeyPoints; upperBodyPoints <- DLC.upperBodyPoints}
###########################################

if (USE_RETRAINED_ONLY) { # if using latest version of DLC only
  setwd('DLC Cam5 Nov 3/')
} else {  # if using both versions to compare or to integrate them
  setwd('DLC Cam5 Apr 7')
}
# setwd('DLC Cam5 May 9') # most recent Spring 2022 version

DLC.fnames <- dir(pattern='^MA.*.csv')  #DLC CSV file names in DLC directory
mm <- match( OP.infantIDs, substr( DLC.fnames,1,5))
if (SUBSET | TEST_COMPARE) 
  DLC.fnames <- DLC.fnames[mm[!is.na(mm)]] # subset that may match

DLC.infantIDs <- substr(DLC.fnames,1,5)
# Read in data ... ~ 1/2 sec per file
header <- read.csv(DLC.fnames[1], skip=1, header=F, nrow=2, row.names=1) # get variable information from lines 2 & 3
DLC.keyPointNames <- header[1,3*1:26] # names of key points: nose, etc. (same for all files)
DLC.dat <- DLC.conf <- list(); DLC.numFrames <- DLC.missing <- numeric() # Storagenumeric() 
cat('\nReading in DLC data:')
# read in all if only DLC; read in only matching if comparing to OP
DLC.dat.col.names <- rep(' ',52)
DLC.dat.col.names[2*(1:26)-1] <- paste(DLC.keyPointNames,'x');
DLC.dat.col.names[2*(1:26)  ] <- paste(DLC.keyPointNames,'y');
for ( ii in 1:length(DLC.fnames)) { # about 20 sec to read in all
  tmp <- read.csv(DLC.fnames[ii],skip=3,header=F,row.names=1)
  cat('\n',ii,':',substr(DLC.fnames[ii],1,5),'has ',dim(tmp)[1],'rows')
  DLC.conf[[ii]] <- as.matrix(tmp[,3*1:26]) # extract confidences
  DLC.dat[[ii]] <- as.matrix(tmp[ ,-(3*1:26)]) # xy positions of key points - drop every 3rd column
  DLC.numFrames[ii] <- dim(tmp)[1]
  # OP and DLC are somewhat inconsistent about reporting dodgy estimates
  # assign confidence 0 to those with missing confidence, missing values, or values 0
  nnn <- DLC.conf[[ii]][,DLC.headKeyPoints] < .1 # select low confidence
  cat('..',dim(tmp)[1],paste('; ',round(100*mean(nnn)),'% head low conf',sep='') )
  DLC.missing[ii] <- round(1000*mean(nnn))/1000 # keep track of missing proportion
  DLC.conf[[ii]][,DLC.headKeyPoints][nnn] <- 0 # set NA confidence values to 0
  tmp <- as.matrix(tmp[ ,-(3*1:26)]) # xy positions of key points - drop every 3rd column
  DLC.numFrames[ii] <- dim(tmp)[1]
  DLC.dat[[ii]] <- array(tmp, dim=c(DLC.numFrames[ii],2,26))

  # DLC.conf[[ii]][ is.na(DLC.conf[[ii]])] <- 0 # if confidence missing set confidence to 0
  # badPoints <- DLC.dat[[ii]][ , 2*1:26-1] == 0 | DLC.dat[[ii]][ , 2*1:26] == 0 # 0 is usually a signal of bad estimate 
  # DLC.conf[[ii]][ badPoints ] <- 0
  # # replace obviously bad values with NA
  # # this code copies out all x columns and then puts them back
  # tmp1 <- DLC.dat[[ii]][ , 2*1:26-1]; tmp1[tmp1==0] <- NA
  # DLC.dat[[ii]][ , 2*1:26-1] <- tmp1 # copy back with NA to flag bad values
  # tmp2 <- DLC.dat[[ii]][ , 2*1:26]; tmp2[tmp2==0] <- NA
  # DLC.dat[[ii]][ , 2*1:26] <- tmp2
  # colnames(DLC.dat[[ii]]) <- DLC.dat.col.names
  # if (2*length(DLC.conf[[ii]])!= length(DLC.dat[[ii]])) cat('\tlength mismatch',ii)
}
setwd('..')

# 
# if (DLC_ONLY) { numFrames <- DLC.numFrames; keyPointNames <- DLC.keyPointNames }
# # timings <- read.csv("Timings_of_events.csv",header=T,row.names=1)
# # outcomes <- read.csv("GMIAindividualMaskBehavior_5JUN18.csv",row.names=1,as.is=T,header=TRUE)
# if (!DLC_ONLY) cat('\n discrepant frame numbers:',infantIDs[numFrames != DLC.numFrames])
# # stop('OK: planned stop')
# if (DLC_ONLY) { # if working only with DLC files; otherwise OP read code handles the following:
# # match up infant motion data with timings and outcomes
# nn1 <- match(infantIDs,rownames(timings)) # check that all match (Dec 12 data has 4 that don't match)
# nn2 <- match(infantIDs,rownames(outcomes)) # Dec 12: 5 don't match
# if (any( is.na(nn1) | is.na(nn2))) {  # shouldn't be any missing
#   warning('\nSome unmatched infants:', c(infantIDs[is.na(nn1)],infantIDs[is.na(nn2)]))
#   nn <- which(!is.na(nn1) & !is.na(nn2))
#   dat <- dat[nn]; conf<-conf[nn]; infantIDs<-infantIDs[nn]
#   nn1 <- match(infantIDs,rownames(timings)) 
#   timings <- timings[nn1,]
#   nn2 <- match(infantIDs,rownames(outcomes)) 
#   outcomes<- outcomes[nn2,]
# }
# }