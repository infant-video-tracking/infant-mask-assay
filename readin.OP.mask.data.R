## Read in OpenPose traces ... 17 sec for 50 infants in directory Feb 28 2022
## the common key point names are those from OpenPose; these will be used to select files from all other versions
# Book-keeping: OP
# NB. There are three header lines in Mike Moore's processed files but only two in others
fps = 30 # frames per second
headKeyPoints = c(1,16:19) # 1:Nose; 16:R Eye; 17:L Eye; 18:R Ear; 19:L Ear
#headColsX <- 2*headKeyPoints - 1; headColsY <- 2*headKeyPoints 
upperBodyPoints <- 2:8  # 2: neck, 3,4,5:Right shoulder, elbow, wrist; 6, 7, 8: Left Shoulder, elbow,  wrist
# if (!exists('OUTCOMES_ONLY')) OUTCOMES_ONLY <- TRUE # if not set
if (TEST_COMPARE) {
  setwd('Hand Annotations/Fall 2021/')
  testfnames <- dir(pattern='^MA.*.csv')
  test.set.IDs <- substr(testfnames,1,5)
  setwd('../..')
}
setwd('OP Cam5 CSVs')
fnames <- dir(pattern='^MA.*.csv') # # OP file names
# if (SUBSET) fnames <- fnames[5*(1:9)-2] # use subset only
if (SUBSET) fnames <- fnames[3*(1:20)-2] # use medium subset only
if (TEST_COMPARE) {
  match.set <- match( test.set.IDs, substr(fnames,1,5))
  fnames <- fnames[match.set]
}
if (OMIT29) {
  mm <- pmatch(c('MA013','MA029'),fnames) # these infant videos not used for QC reasons
  if (length(mm)>0) 
    fnames <- fnames[setdiff(1:length(fnames),mm)]
}
infantIDs <- substr(fnames,1,5)

#### Read in data ... ~ 1 sec per file
header <- read.csv(fnames[1], header=F, nrow=2, row.names=1) # get variable information from lines 2 & 3
keyPointNames <- header[2,3*1:25-1] # names of key points: nose, etc. (same for all files)
dat <- conf <- confSum <- list(); OP.numFrames <- OP.missing <- numeric() # Storage
cat('\nReading in OpenPose data:\n')
for ( ii in 1:length(fnames)) {  
  tmp <- read.csv(fnames[ii],skip=3,header=F,row.names=1)
  cat('\n',ii,infantIDs[ii])
  conf[[ii]] <- as.matrix(tmp[,3*1:25]) # extract confidences
  nnn <- is.na(conf[[ii]])
  cat('..',dim(tmp)[1],paste(' frames; ', round(100*mean(nnn)),'% NA',sep='') )
  OP.missing[ii] <- round(1000*mean(nnn))/1000 # keep track of missing
  conf[[ii]][nnn] <- 0 # set NA confidence values to 0
  tmp <- as.matrix(tmp[ ,-(3*1:25)]) # xy positions of key points - drop every 3rd column
  OP.numFrames[ii] <- dim(tmp)[1]
  dat[[ii]] <- array(tmp, dim=c(OP.numFrames[ii],2,25))
}
cat('\nFinished reading',date()) # Timing
OP.dat <- dat; OP.conf <- conf; OP.keyPointNames <- keyPointNames
OP.headKeyPoints <- headKeyPoints; OP.upperBodyPoints <- upperBodyPoints
OP.infantIDs <- infantIDs; 
rm( list = c('infantIDs','conf','dat'))
gc()
setwd('..')
